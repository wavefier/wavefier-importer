import os
import time

if os.environ['IMPORTER_FILE_SYSTEM'] == 'NETWORK':
    from watchdog.observers.polling import PollingObserver as Observer
else:    
    from watchdog.observers import Observer

from watchdog.events import FileSystemEventHandler
from wavefier_importer.Sender import Sender
import logging

logging.basicConfig(level=logging.INFO)


TRIGGER_EVENT = os.environ['TRIGGER_EVENT']

class Watcher:
    """
        This class can be used to wait until a new file is created on observed directory
    """
    def __init__(self, path):
        self.__observer = Observer()
        self.__path = path

    def run(self):
        """
            Specify the directory to watch and define the handler to manage the action to do
        """
        event_handler = Handler()
        self.__observer.schedule(event_handler, self.__path, recursive=False)
        self.__observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.__observer.stop()

        self.__observer.join()


class Handler(FileSystemEventHandler):
    """
        Handler to manage an action to do when a file is created on observed directory
    """
    @staticmethod
    def on_any_event(event):
        """
            method tho invoke when an event is raised

            :param event: the event registred on specific directory (created, modified, canceled)
        """
        if event.is_directory:
            return None
    
        elif TRIGGER_EVENT == 'MOVE' and event.event_type == 'moved':
            logging.info("New file created: %s", event.dest_path)
            sender.push(event.dest_path)

        elif TRIGGER_EVENT == 'CREATE' and event.event_type == 'created':
            logging.info("New file created: %s", event.src_path)
            sender.push(event.src_path)


if __name__ == '__main__':
    broker = os.environ['KAFKA_BROKER']
    path = os.environ['DIR_MOUNT']
    sender = Sender(path, broker)
    sender.start()
    w = Watcher(path)
    w.run()
