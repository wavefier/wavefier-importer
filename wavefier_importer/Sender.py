import os

from wavefier_common.kafka.Producer import KafkaProducer
from wavefier_common.util.Path import Path

from raw.RawExtractor import RawExtractor
import logging
from concurrent.futures import ThreadPoolExecutor

logging.basicConfig(level=logging.INFO)

class Sender:
    """This class send a gwf file in Kafka when it has been created into an observed directory on filesystem
    """
    def __init__(self, path, broker):
        self.__broker = broker
        self.__path = path
        self.__producer = None
        self.__executor = ThreadPoolExecutor(max_workers=10)

    def start(self):
        """
            Find and push on Kafka, files already present in the observed directory
        """
        self.__producer = KafkaProducer({'bootstrap.servers': self.__broker})
        if self.__producer is None:
            logging.error("Error connecting with broker: %s", self.__broker)
            return
        else:
            logging.info("Broker connected: %s", self.__broker)

        if os.path.isdir(self.__path) and (os.path.exists(self.__path)):
            script_dir = os.path.abspath(os.path.dirname(__file__))
            file_path = str(Path(script_dir) / self.__path)
            os.chdir(file_path)

            # files variable contains all files and folders under the path directory
            files = sorted(os.listdir(os.getcwd()), key=os.path.getmtime, reverse=True)

            for f in files:
                if os.path.isfile(f):
                    self.push(f)

    def push(self, filename):
        self.__executor.submit(self.publish_message, filename)

    def publish_message(self, filename):
        """
            Push on kafka the content of filename passed

            :param filename: filename of file to push
        """
        logging.info("New file to push: %s", filename)
        extension = filename.split(".")[-1]
        if extension.upper() == "GWF":
            logging.info("Extension ok")
            sve = RawExtractor(filename)
            message = sve.extract_message()
            logging.info("Created message")
            self.__producer.publish_message(message)
            logging.info("Message published")
        else:
            logging.error("File extension not equals to GWF: %s", extension.upper())
