#!/bin/bash

TOPIC_NAME=$2
KAFKA_HOST=$1:2181
KAFKA_WP=/opt/kafka_2.11-0.10.2.2

CMD="kafka-topics.sh --create --zookeeper ${KAFKA_HOST}  --replication-factor 1  --partitions 1 --topic ${TOPIC_NAME}"

${KAFKA_WP}/bin/${CMD}
