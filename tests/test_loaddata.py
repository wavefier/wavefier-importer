import os
from unittest import TestCase
from wavefier_common.RawData import RawData
from wavefier_common.kafka.Consumer import KafkaConsumer

from wavefier_importer.Sender import Sender


class RawDataTest(TestCase):

    def test_loaddata(self):
        broker = "kafka_test:9092"
        os.environ['KAFKA_BROKER'] = broker
        script_dir = os.path.abspath(os.path.dirname(__file__))
        subdir = os.path.join(script_dir, 'test_static_data')

        sender = Sender(subdir)
        sender.start()

        consumer = KafkaConsumer({
            'bootstrap.servers': broker,
            'group.id': 'testGroups',
            'default.topic.config': {
                'auto.offset.reset': 'smallest'
            }
        })

        message = RawData("a-a-11.gwf", None)
        consumer.subscribe(message)
        msg_consumed = consumer.consume_message()

        self.assertEqual("V-aaa-1222593214.gwf", msg_consumed.getFilename())
