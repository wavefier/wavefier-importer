Welcome to wavefier-importer's documentation!
========================================

The wavefier_importer module is part of the Wavefier project. This module contains all the code to retrieve the Raw Data in the wavefier filesystem.


Table of content
=================

.. toctree::
   :maxdepth: 2

   structure/introduction
   structure/installation
   structure/use_it
   structure/develop_it
   structure/apidoc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
