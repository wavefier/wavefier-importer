if [ $1 ]; then
  IMAGE_NAME=$1
  IMAGE_VERSION=$(eval head -n1 ${IMAGE_NAME}/version.txt)
else
  echo "The argument IMAGE_NAME is reqired!"
  exit
fi

GITLAB_PROJECT="${2:-}"


if [ $GITLAB_PROJECT == ""]; then 
  docker push wavefier/${IMAGE_NAME}:${IMAGE_VERSION}
else
  docker push ${GITLAB_PROJECT}:${IMAGE_NAME}_${IMAGE_VERSION}
fi
