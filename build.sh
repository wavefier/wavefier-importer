if [ $1 ]; then
  IMAGE_NAME=$1
  IMAGE_VERSION=$(eval head -n1 ${IMAGE_NAME}/version.txt)
else
  echo "The argument IMAGE_NAME is reqired!"
  exit
fi

GITLAB_PROJECT="${2:-empty}"
FROM_IMAGE="${3:-empty}"

echo "GITLAB_PROJECT=" $GITLAB_PROJECT
echo "FROM_IMAGE=" $FROM_IMAGE
echo "IMAGE_NAME=" $IMAGE_NAME

if [ $GITLAB_PROJECT == "empty" ]; then 
  docker build -t wavefier/${IMAGE_NAME}:${IMAGE_VERSION} ${IMAGE_NAME}
elif [ $FROM_IMAGE != "empty" ]; then
  docker build -t ${GITLAB_PROJECT}:${IMAGE_NAME}_${IMAGE_VERSION} \
               --build-arg FROM_IMAGE=${FROM_IMAGE} \
                ${IMAGE_NAME}
else
  docker build -t ${GITLAB_PROJECT}:${IMAGE_NAME}_${IMAGE_VERSION} \
                ${IMAGE_NAME}

fi
