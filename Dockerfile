FROM wdfteam/wdfpipe:2.0.0

WORKDIR /importer

COPY .  /importer/.

RUN apt-get --force-yes update
#RUN apt-get --assume-yes install librdkafka1
RUN apt-get --assume-yes install librdkafka-dev


RUN python setup.py install

CMD ["python", "-u", "wavefier_importer/WatcherFiles.py"]
