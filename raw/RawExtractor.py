
from wavefier_common.RawData import RawData


class RawExtractor:
    """
        This class provide to create a representation of RawData containing the data presents on filename
    """
    def __init__(self, filename: str):
        self.__filename = filename

    def extract_message(self):
        """
            Extract the data from the file and return a RawData structure

            :return a RawData representation with filename and data in bytes
        """
        f = open(self.__filename, "rb")
        return RawData(self.__filename, f.read())


