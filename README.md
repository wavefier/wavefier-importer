# Wavefier Disk to Kafka Importer
The purpose of this module is to create a mechanism to importe files within the **Wavefier** pipeline. 
Using Wavefier Disk to Kafka Importer it is possible to read from filesystem files (1sec). 
It is possible configure the module via **docker-compose** exploiting environment variables:


* **DIR_MOUNT** Indicate the directory to watch in order to trigger new event
* **KAFKA_BROKER** Indicate the address of KAFKA broker to put the file gathered from the filesystem
* **TRIGGER_EVENT** CREATED or MOVE. Indicate dthe event that trigger the recognition of new file in filesyestm
* **IMPORTER_FILE_SYSTEM** NETWORK or LOCAL. Indicate the type of filesystem used by importer

